import socket, os, uuid 
from flask import Flask, request, jsonify 

os.environ["UUID"] = str(uuid.uuid4())

app = Flask(__name__)

@app.route("/hostname")
def get_hostname():
    return jsonify(socket.gethostname())

@app.route("/author")
def get_author():
    return jsonify(os.getenv("AUTHOR"))

@app.route("/id")
def get_uuid():
    return jsonify(os.getenv("UUID"))


if (__name__) == "__main__":
    app.run(debug=True, port=8000)